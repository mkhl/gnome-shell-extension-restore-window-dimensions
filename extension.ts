import '@girs/gnome-shell'
import '@girs/gjs/dom'

import Gio from '@girs/gio-2.0'
import GLib from '@girs/glib-2.0'
import Meta from '@girs/meta-14'
import Shell from '@girs/shell-14'

type Rect = {
	x?: number
	y?: number
	width?: number
	height?: number
}
type CondRect = Rect & {
	[k: string]: Rect | undefined
}
type Config = Record<string, CondRect>

class Resizer implements Rect {
	private rects: Rect[]
	constructor(...rects: (Rect | undefined)[]) {
		this.rects = rects.filter((r): r is Rect => r !== undefined)
	}

	private get(k: keyof Rect): number | undefined {
		return this.rects.map((r) => r[k]).find((v) => v !== undefined)
	}

	get x(): number | undefined {
		return this.get('x')
	}
	get y(): number | undefined {
		return this.get('y')
	}
	get width(): number | undefined {
		return this.get('width')
	}
	get height(): number | undefined {
		return this.get('height')
	}
}

export default class Extension {
	private config: Config = {}
	private decoder = new TextDecoder('utf-8')
	private configFile = Gio.File.new_for_path(
		GLib.build_filenamev([
			GLib.get_user_config_dir(),
			'restore-window-dimensions',
			'config.json',
		]),
	)

	enable() {
		const [ok, config] = this.configFile.load_contents(null)
		this.config = ok ? JSON.parse(this.decoder.decode(config)) : {}
		this.run()
	}

	disable() {
		this.config = {}
	}

	run() {
		this.windows().forEach((win) => this.resize(win))
	}

	windows() {
		const wm = Shell.Global.get().workspace_manager
		return Array.from(Array(wm.get_n_workspaces()).keys())
			.map((n) => wm.get_workspace_by_index(n))
			.flatMap((ws) => ws?.list_windows() ?? [])
			.filter((win) => win.get_window_type() === Meta.WindowType.NORMAL)
	}

	resize(window: Meta.Window) {
		if (!window.wm_class) return
		const resize = this.resizer(window)
		if (!resize) return
		console.log('restoring window', window.get_title())
		const rect = window.get_frame_rect()
		const display = window.get_display()
		const scale = display.get_monitor_scale(window.get_monitor())
		const monitor = display.get_monitor_geometry(window.get_monitor())
		const x = resize.x == null ? rect.x : resize.x * scale + monitor.x
		const y = resize.y == null ? rect.y : resize.y * scale + monitor.y
		const w = resize.width == null ? rect.width : resize.width * scale
		const h = resize.height == null ? rect.height : resize.height * scale
		window.move_resize_frame(false, x, y, w, h)
	}

	resizer(window: Meta.Window): Resizer | undefined {
		if (!window.wm_class) return
		const config = this.config[window.wm_class]
		if (!config) return
		const display = window.get_display()
		const scale = display.get_monitor_scale(window.get_monitor())
		const monitor = display.get_monitor_geometry(window.get_monitor())
		const key = `${monitor.width / scale}x${monitor.height / scale}`
		return new Resizer(config[key], config)
	}
}

function init() {
	return new Extension()
}
