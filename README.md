# Restore Window Dimensions - GNOME Shell Extension

## What?

- Install the extension

- Create `$HOME/.config/restore-window-dimensions/config.json`

- Configure some window sizes and/or positions by wmclass, for example:

  ```json
  {
  	"firefox": {
  		"width": 1234,
  		"height": 789,
  		"x": 78,
  		"y": 69
  	}
  }
  ```

  You can find each window's wmclass with looking glass:
  Open the `Run` dialog, enter `lg`, and check out the "Windows" tab

- Enable the extension to apply the configured dimensions
